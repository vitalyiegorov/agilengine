import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {map} from 'rxjs/operators';
import {merge, Observable} from 'rxjs';

import {TextService} from '../text-service/text.service';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileComponent implements OnInit {
  public text$: Observable<string[]>;

  constructor(public textService: TextService,
              private cdRef: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.text$ = this.textService.getMockText().pipe(
      map(text => text.trim().split(' ').filter(a => !/\s+/.test(a))),
    );

    merge(this.textService.selectStyle$, this.textService.selectedWord$)
      .subscribe(() => this.cdRef.detectChanges());
  }
}

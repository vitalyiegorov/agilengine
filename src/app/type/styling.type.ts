import {StylingEnum} from '../enum/styling.enum';

export type StylingType =
  | StylingEnum.BOLD
  | StylingEnum.ITALIC
  | StylingEnum.UNDERLINE
  | StylingEnum.COLOR
  | StylingEnum.SYNONYM;

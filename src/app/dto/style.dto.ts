import {StylingType} from '../type/styling.type';

export class StyleDTO {
  constructor(public command: StylingType,
              public parameters?: any[]) {
  }
}

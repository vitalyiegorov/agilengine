import {ChangeDetectionStrategy, ChangeDetectorRef, Component, HostBinding, HostListener, Input} from '@angular/core';
import {TextService} from '../text-service/text.service';

@Component({
  selector: 'app-word',
  template: `{{ word }}`,
  styleUrls: ['./word.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WordComponent {
  @Input() @HostBinding('class.bold') isBold = false;
  @Input() @HostBinding('class.italic') isItalic = false;
  @Input() @HostBinding('class.underline') isUnderline = false;
  @Input() @HostBinding('class.selected') isSelected = false;
  @Input() @HostBinding('style.color') color = 'black';
  @Input() word = '';

  @HostListener('click') handleClick() {
    this.textService.selectWord(this);
  }

  constructor(private cdRef: ChangeDetectorRef, private textService: TextService) {
  }

  update() {
    this.cdRef.detectChanges();
  }
}

import {ChangeDetectionStrategy, Component} from '@angular/core';

import {TextService} from '../text-service/text.service';
import {StylingEnum} from '../enum/styling.enum';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent {
  public stylingEnum = StylingEnum;
  public colors = ['black', 'red', 'green'];

  constructor(public textService: TextService) {
  }
}
